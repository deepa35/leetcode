public class AddBinary {
    public static String addBinaryOptimal(String a, String b) {
        if(b == null || b.length() == 0 || b.equals("0")) {
            return a;
        }

        if(a == null || a.length() == 0 || a.equals("0")) {
            return b;
        }

        int aLen = a.length()-1;
        int bLen = b.length()-1;
        int sum = 0;
        String out = "";

        while(aLen>=0 || bLen>=0 || sum == 1) {
            sum+= aLen>=0? a.charAt(aLen)-'0':0;
            sum+= bLen>=0? b.charAt(bLen)-'0':0;
            out = sum%2 +out;
            sum = sum/2;
            aLen--;
            bLen--;

        }
        return out;
    }

    public static String addBinary(String a, String b) {
        if(b == null || b.length() == 0 || b.equals("0")) {
            return a;
        }

        if(a == null || a.length() == 0 || a.equals("0")) {
            return b;
        }

        int aLen = a.length()-1;
        int bLen = b.length()-1;
        int carry = 0;
        String out = "";

        while(aLen>=0 && bLen >=0) {
            if(a.charAt(aLen) == '1' && b.charAt(bLen) == '1') {
                if(carry == 1) {
                    out = "1"+out;
                } else {
                    out = "0"+out;
                }
                carry = 1;
            } else if ((a.charAt(aLen) == '1' && b.charAt(bLen) == '0') ||
                    (a.charAt(aLen) == '0' && b.charAt(bLen) == '1')) {
                if(carry == 1) {
                    out = "0" + out;
                    carry = 1;
                } else {
                    out = "1" + out;
                }
            } else {
                if(carry == 1) {
                    out = "1" + out;
                    carry = 0;
                } else {
                    out = "0" + out;
                }
            }
            aLen--;
            bLen--;
        }

        String numStr = "";
        int len = -1;

        if(aLen != -1) {
           len = aLen;
           numStr = a;
        } else if(bLen != -1) {
            len = bLen;
            numStr = b;
        }

        while(len != -1) {
            if(numStr.charAt(len) == '0') {
                out = carry + out;
                carry = 0;
            } else {
                if(carry == 1) {
                    out = "0" + out;
                } else {
                    out = "1" + out;
                }
            }
            len --;
        }


        if(carry == 1) {
            out = "1"+out;
        }
        return out;
    }

    public static void main(String[] args) {
        System.out.println(addBinaryOptimal("100","1100"));
    }
}
