public class AddTwoNumbers {
    public static ListNode addTwoNumbersRecursive(ListNode l1, ListNode l2) {
        ListNode ans = new ListNode(0);
        helper(ans,l1,l2,0);
        return ans.next;
    }

    private static void helper(ListNode l1, ListNode l2, ListNode ans, int carry) {
        if(l1==null && l2==null && carry == 0) {
            return;
        }

        int sum = 0;
        sum = l1!=null?sum+l1.val:sum;
        sum = l2!=null?sum+l2.val:sum;
        sum+=carry;

        if(sum>9) {
            carry = sum/10;
            sum = sum%10;
        }

        ans.next = new ListNode(sum);
        if(l1==null && l2==null) {
            helper(null,null,ans.next,carry);
        } else if(l1==null) {
            helper(null,l2.next,ans.next,carry);
        } else if(l2==null) {
            helper(l1.next,null,ans.next,carry);
        } else {
            helper(l1.next, l2.next, ans.next, carry);
        }
    }

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int carry = 0;
        ListNode first = l1;
        ListNode second = l2;
        ListNode answer = new ListNode(0);
        ListNode temp = answer;

        while(first!=null || second!=null) {
            int sum = 0;
            sum = first!=null? sum+first.val:sum;
            sum = second!=null? sum+second.val:sum;
            sum+=carry;
            carry = 0;
            if(sum>9) {
                carry = sum/10;
                sum = sum%10;
            }
            temp.next = new ListNode(sum);
            temp = temp.next;
            first = first!=null?first.next: first;
            second = second!=null?second.next:second;
        }

        if(carry == 1) {
            temp.next = new ListNode(carry);
        }

        return answer.next;
    }

    public static void main(String[] args) {
        ListNode a = new ListNode(2);
        a.next = new ListNode(4);
        a.next.next = new ListNode(3);

        ListNode b = new ListNode(5);
        b.next = new ListNode(9);
        b.next.next = new ListNode(2);

        addTwoNumbersRecursive(a,b);

    }
}
