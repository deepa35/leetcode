import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AmazonAirShipRoutes {
    public static List<List<Integer>> airRoutes(List<List<Integer>> forw, List<List<Integer>> ret, int flight) {
        int[][] routeTab = new int[forw.size()][ret.size()];
        List<List<Integer>> out = new ArrayList<>();
        int min = Integer.MAX_VALUE;

        for(int i=0;i<routeTab.length;i++) {
            for(int j=0;j<routeTab[0].length;j++) {
                routeTab[i][j] = flight-(forw.get(i).get(1) + ret.get(j).get(1));
                if(routeTab[i][j]>=0 && routeTab[i][j] <min) {
                    min = routeTab[i][j];
                }
            }
        }

        for(int i=0;i<routeTab.length;i++) {
            for(int j=0;j<routeTab[0].length;j++) {
                if(routeTab[i][j] == min) {
                    out.add(new ArrayList<>(Arrays.asList(i+1,j+1)));
                }
            }
        }

        return out;
    }

    public static void main(String[] args) {
        List<List<Integer>> forw = new ArrayList<>();
        List<List<Integer>> rev = new ArrayList<>();

        forw.add(new ArrayList(){{add(1); add(3000);}});
        forw.add(new ArrayList(){{add(2); add(5000);}});
        forw.add(new ArrayList(){{add(3); add(7000);}});
        forw.add(new ArrayList(){{add(4); add(10000);}});

        rev.add(new ArrayList(){{add(1); add(2000);}});
        rev.add(new ArrayList(){{add(2); add(3000);}});
        rev.add(new ArrayList(){{add(3); add(4000);}});
        rev.add(new ArrayList(){{add(4); add(5000);}});

        for(List<Integer> each: airRoutes(forw,rev,10000)) {
            System.out.println(each.get(0)+","+each.get(1));
        }
    }
}
