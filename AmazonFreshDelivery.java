import java.util.*;

public class AmazonFreshDelivery {
    public static List<List<Integer>> deliveryLocs(List<List<Integer>> location, int numDest) {
        int numLocs = location.size();
        Collections.sort(location, new LocSorter());
        List<List<Integer>> out = new ArrayList<>();

        for(int i=0;i<numDest;i++) {
            out.add(location.get(i));
        }
        return out;
    }

    public static void main(String[] args) {
        List<List<Integer>> loc = new ArrayList<>();
        loc.add(new ArrayList<Integer>(){{add(1); add(2);}});
        loc.add(new ArrayList<Integer>(){{add(3); add(4);}});
        loc.add(new ArrayList<Integer>(){{add(1); add(-1);}});

        for(List<Integer> each: deliveryLocs(loc,2)) {
            System.out.println(each.get(0)+","+each.get(1));
        }
    }
}

class LocSorter implements Comparator<List>{
    @Override
    public int compare(List o1, List o2) {
        double val1 = Math.pow((int)o1.get(0),2)+Math.pow((int)o1.get(1),2);
        double val2 = Math.pow((int)o2.get(0),2)+Math.pow((int)o2.get(1),2);
        return Double.compare(Math.sqrt(val1), Math.sqrt(val2));
    }
}
