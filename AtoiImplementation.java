
public class AtoiImplementation {
    public static int myAtoi(String str) {
        if(str == null) {
            return 0;
        }
        int index = 0;
        boolean isNeg = false;
        int ans = 0;

        String strNew = str.trim();

        if(strNew.length() == 0) {
            return 0;
        }

        if(strNew.charAt(0) == '-' || strNew.charAt(0) == '+') {
            isNeg = strNew.charAt(0) == '-'? true:false;
            index++;
        }

        while(index<strNew.length()) {
            if(!Character.isDigit(strNew.charAt(index))) {
                break;
            }
            int digit = strNew.charAt(index) - '0';
            if(Integer.MAX_VALUE/10 < ans ||(Integer.MAX_VALUE/10==ans && Integer.MAX_VALUE%10 < digit)) {
                return isNeg?Integer.MIN_VALUE:Integer.MAX_VALUE;
            }
            ans = ans*10+digit;
            index++;
        }
        return isNeg? -1*ans: ans;
    }

    public static void main(String[] args) {
        System.out.println(myAtoi("   +423jhhj  "));

    }
}
