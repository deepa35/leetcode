public class BalancedBinaryTree {
    private static boolean result = true;
    public static boolean isBalancedOptimal(TreeNode root) {
       if(root == null) {
           return result;
       }
       getHeight(root);
       return result;
    }

    private static int getHeight(TreeNode root) {
        if(root == null) {
            return 0;
        }

        int left = getHeight(root.left);
        int right = getHeight(root.right);

        if(Math.abs(left-right) > 1) {
            result=false;
        }

        return 1+Math.max(left,right);
    }


    public static boolean isBalanced(TreeNode root) {
        if(root == null) {
            return true;
        }
        int leftHt = height(root.left);
        int rightHt = height(root.right);

        if(Math.abs(leftHt-rightHt) > 1) {
            return false;
        }
        return isBalanced(root.left) && isBalanced(root.right);
    }

    private static int height(TreeNode node) {
        if(node == null) {
            return 0;
        }
        return 1+Math.max(height(node.right), height(node.left));
    }
    public static void main(String[] args) {
        TreeNode node = new TreeNode(3);
        node.left = new TreeNode(9);
        node.right = new TreeNode(20);
        node.right.left = new TreeNode(15);
        node.right.right = new TreeNode(7);
        System.out.println(isBalancedOptimal(node));
    }
}
