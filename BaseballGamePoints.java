import java.util.Stack;

public class BaseballGamePoints {
    public static int calPoints(String[] ops) {
        int total = 0;
        Stack<Integer> tracker = new Stack<>();
        for(int i=0;i<ops.length;i++) {
            if(ops[i].equals("+")) {
                int prev = tracker.pop();
                int newTop = prev + tracker.peek();
                tracker.push(prev);
                tracker.push(newTop);
            } else if(ops[i].equals("C")) {
                tracker.pop();
            } else if(ops[i].equals("D")) {
                tracker.push(2*tracker.peek());
            } else {
                tracker.push(Integer.valueOf(ops[i]));
            }
        }

        for(int each:tracker) {
            total+=each;
        }
        return total;
    }

    public static void main(String[] args) {
        System.out.println(calPoints(new String[]{"5","2","C","D","+"}));
    }
}
