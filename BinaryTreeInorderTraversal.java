import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class BinaryTreeInorderTraversal {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> out = new ArrayList<>();
        helper(root,out);
        return out;
    }

    private void helper(TreeNode node,List<Integer> out) {
        if(node==null) {
            return;
        }
        helper(node.left,out);
        out.add(node.val);
        helper(node.right,out);
    }

    public List<Integer> inorderTraversalIterative(TreeNode root) {
        List<Integer> out = new ArrayList<>();
        Stack<TreeNode> help = new Stack<>();
        while(root!=null || !help.isEmpty()) {
            while(root!=null) {
                help.push(root);
                root=root.left;
            }
            TreeNode pop = help.pop();
            out.add(pop.val);
            root=pop.right;
        }
        return out;
    }
}
