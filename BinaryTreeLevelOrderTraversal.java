import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class BinaryTreeLevelOrderTraversal {
    public static List<List<Integer>> levelOrderBottomOptimal(TreeNode root) {
        List<List<Integer>> out = new ArrayList<>();
        lobOptimalHelper(out, root, 0);
        Collections.reverse(out);
        return out;
    }

    private static void lobOptimalHelper(List<List<Integer>> out, TreeNode root, int level) {
        if(root == null) {
            return;
        }
        if(out.size()<=level) {
            out.add(new ArrayList<>());
        }
        out.get(level).add(root.val);
        lobOptimalHelper(out,root.left,level+1);
        lobOptimalHelper(out,root.right,level+1);
    }


    public static List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> out = new LinkedList<>();
        lobHelper(root,out,0);
        return out;
    }

    private static void lobHelper(TreeNode node, List<List<Integer>> out, int level) {
        if(node == null) {return;}
        if(out.size()-1<level) {
            ((LinkedList<List<Integer>>) out).addFirst(new LinkedList<>());
        }
        out.get(out.size()-1-level).add(node.val);
        lobHelper(node.left,out,level+1);
        lobHelper(node.right,out,level+1);
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(9);
        root.right = new TreeNode(20);
        root.right.left = new TreeNode(15);
        root.right.right = new TreeNode(7);
        System.out.println(levelOrderBottomOptimal(root));
    }
}
