public class BuyAndSellStock1 {
    public static int maxProfitOptimal(int[] prices) {
        int minPrice = Integer.MAX_VALUE;
        int maxProfit = 0;
        for(int i=0;i<prices.length;i++) {
            if(prices[i]<minPrice) {
                minPrice = prices[i];
            }
            if(prices[i]-minPrice > maxProfit) {
                maxProfit = prices[i]-minPrice;
            }
        }
        return maxProfit;
    }
    public static int maxProfit(int[] prices) {
        int maxProf = 0;
        for(int i=0;i<prices.length-1;i++) {
            for(int j=i+1;j<prices.length;j++) {
                if(prices[j]-prices[i] > maxProf) {
                    maxProf = prices[j]-prices[i];
                }
            }
        }
        return maxProf;
    }

    public static void main(String[] args) {
        System.out.println(maxProfitOptimal(new int[]{7,1,5,3,6,4}));
    }
}
