import java.util.ArrayList;

public class ClimbingStairs {

    public static int climbStairs(int n) {
        if(n==1) {
            return 1;
        }
        if(n==2) {
            return 2;
        }

        //ArrayList<Integer> store = new ArrayList<>();
        int[] store = new int[n];
        store[0] = 1;
        store[1] = 2;

        for(int i=2; i<n;i++) {
            store[i] = store[i-1] + store[i-2];
        }
        return store[n-1];

    }
    public static void main(String[] args) {
        System.out.println(climbStairs(3));
    }
}
