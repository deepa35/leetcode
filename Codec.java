import java.util.HashMap;
import java.util.Map;

public class Codec {
    Map<Integer,String> codec = new HashMap<>();

    // Encodes a URL to a shortened URL.
    public String encode(String longUrl) {
        int codeURL = longUrl.hashCode();
        codec.put(codeURL, longUrl);
        return "http://tinyurl.com/"+codeURL;
    }

    // Decodes a shortened URL to its original URL.
    public String decode(String shortUrl) {
        shortUrl = shortUrl.replace("http://tinyurl.com/","");
        return codec.get(Integer.valueOf(shortUrl));
    }
}
