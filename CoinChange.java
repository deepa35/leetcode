import java.util.Arrays;

public class CoinChange {
    public static int coinChange(int[] coins, int amount) {
        int maxVal = amount+1;
        int[] coinCount = new int[amount+1];
        Arrays.fill(coinCount,maxVal);
        coinCount[0]=0;
        for(int i=1;i<coinCount.length;i++) {
            for(int j=0;j<coins.length;j++) {
                if(coins[j]<=i) {
                    coinCount[i] = Math.min(coinCount[i],coinCount[i-coins[j]]+1);
                }
            }
        }
        return coinCount[amount]>amount?-1:coinCount[amount];
    }

    public static void main(String[] args) {
        coinChange(new int[]{1,2,5},11);
    }
}
