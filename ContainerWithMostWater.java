public class ContainerWithMostWater {
    public static int maxAreaOptimal(int[] height) {
        int max=0;
        int left =0;
        int right=height.length-1;

        while(left<right) {
            int area= Math.min(height[left], height[right]) * (right-left);
            if(area>max) {
                max = area;
            }
            if(height[left]<height[right]) {
                left++;
            } else {
                right--;
            }
        }
        return max;
    }
    public static int maxArea(int[] height) {
        int max = 0;
        for(int i=0;i<height.length;i++) {
            for(int j=i+1;j<height.length;j++) {
                int area = Math.min(height[i], height[j]) * (j-i);
                if(area>max) {
                    max = area;
                }
            }
        }
        return max;
    }
    public static void main(String[] args) {
        System.out.println(maxAreaOptimal(new int[]{1,8,6,2,5,4,8,3,7}));
    }
}
