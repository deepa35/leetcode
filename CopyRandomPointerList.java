import java.util.HashMap;
import java.util.Map;

class RandomListNode {
    int label;
    RandomListNode next, random;
    RandomListNode(int x) { this.label = x; }
}

public class CopyRandomPointerList {
    Map<RandomListNode,RandomListNode> vistedNodes = new HashMap<>();

    public RandomListNode copyRandomList(RandomListNode head) {
        if(head==null) {
            return null;
        }
        if(vistedNodes.containsKey(head)) {
            return vistedNodes.get(head);
        }

        RandomListNode newNode = new RandomListNode(head.label);
        vistedNodes.put(head,newNode);
        newNode.next = copyRandomList(head.next);
        newNode.random = copyRandomList(head.random);
        return newNode;
    }
}
