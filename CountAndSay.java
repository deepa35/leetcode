public class CountAndSay {
    public static String countAndSay(int n) {
        if(n==1) {
            return "1";
        }
        String input = "1";
        String out = "";
        for(int i=2;i<=n;i++) {
            out = "" ;
            int counter = 1;
            char elem;
            char prevElem = input.charAt(0);
            for(int j=1;j<input.length();j++) {
                elem = input.charAt(j);
                if(elem == prevElem) {
                    counter++;
                } else {
                    out= out+ counter+prevElem;
                    counter = 1;
                    prevElem = elem;
                }
            }
            out=out+counter+prevElem;
            input = out;
        }
        return out;
    }
    
    public static String countAndSayAlt(int n) {
        if(n==1) {
            return "1";
        }
        String input = "1";
        StringBuilder out = null;
        for(int i=2;i<=n;i++) {
            out = new StringBuilder() ;
            int counter = 1;
            char elem;
            char prevElem = input.charAt(0);
            for(int j=1;j<input.length();j++) {
                elem = input.charAt(j);
                if(elem == prevElem) {
                    counter++;
                } else {
                    out.append(counter);
                    out.append(prevElem);
                    counter = 1;
                    prevElem = elem;
                }
            }
            out.append(counter);
            out.append(prevElem);
            input = out.toString();
        }
        return out.toString();
    }

    public static void main(String[] args) {
        System.out.println(countAndSay(2));
    }
}
