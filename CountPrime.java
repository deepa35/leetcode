public class CountPrime {

    public static int countPrimes(int n) {
        int numPrimes = 0;
        boolean[] nonPrimeNums = new boolean[n];
        for(int i=2; i<n;i++) {
            if(nonPrimeNums[i] == false) {
                numPrimes++;
                for(int j= 2; i*j<n;j++) {
                    nonPrimeNums[i*j] = true;
                }
            }
        }
        return numPrimes;
    }
    public static void main(String[] args) {
        System.out.println(countPrimes(10));
    }
}
