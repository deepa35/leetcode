public class DCGCalculator {

    private static double DCG(double[] arr) {
        double answer = 0;
        for(int i=0;i<arr.length;i++) {
            answer+= (arr[i] * Math.log(2)/Math.log(i+2));
        }
        return answer;
    }
    public static void main(String[] args) {
        System.out.println(DCG(new double[]{3,2,3,0,1,2}));
    }
}
