public class DIStringMatch {
    public int[] diStringMatch(String S) {
        int Iindex = 0;
        int Dindex = S.length();
        int[] out = new int[S.length()+1];

        for(int i=0;i<S.length();i++) {
            if(S.charAt(i)=='I') {
                out[i] = Iindex++;
            } else {
                out[i] = Dindex--;
            }
        }
        if(S.charAt(S.length()-1)=='I') {
            out[S.length()]=++Iindex;
        } else{
            out[S.length()]=--Dindex;
        }

        return out;

    }
}
