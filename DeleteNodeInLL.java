public class DeleteNodeInLL {
    public void deleteNode(ListNode node) {
        if(node==null || node.next ==null) {
            return;
        }
        ListNode prev=null;

        while(node.next!=null) {
            node.val = node.next.val;
            prev=node;
            node = node.next;
        }
        prev.next = null;
    }
}
