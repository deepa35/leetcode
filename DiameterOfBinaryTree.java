public class DiameterOfBinaryTree {
    int diameter = 0;

    public int diameterOfBinaryTree(TreeNode root) {
        diameterHelper(root);
        return diameter;
    }

    private int diameterHelper(TreeNode root) {
        if(root == null) {
            return 0;
        }

        int L = diameterHelper(root.left);
        int R = diameterHelper(root.right);

        diameter = Math.max(L+R,diameter);
        return 1+Math.max(L,R);
    }
}
