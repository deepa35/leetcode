public class DivideTwoNumbers {

    public static int divideOptimal(int dividend, int divisor) {
        if(Integer.MIN_VALUE==dividend && divisor==-1) {return Integer.MAX_VALUE;}
        if(dividend==0) { return 0;}
        boolean isNeg = (dividend<0||divisor<0) && !(dividend<0 && divisor<0)? true:false;
        dividend = dividend>0?-dividend:dividend;
        divisor = divisor>0?-divisor:divisor;
        int out = divideHelper(dividend,divisor);
        return isNeg?-out:out;
    }

    private static int divideHelper(int dend, int sor) {

        int result = sor;
        int prev = 0;
        int count = 0;
        while(dend<=result) {
            prev=result;
            result+=result;
            count=count==0?1:count+count;
            if(result>prev){break;}
        }
        return count==0? count:count+divideHelper(dend-prev,sor);
    }


    //BF
    public static int divide(int dividend, int divisor) {
        if(dividend == 0) {
            return 0;
        }

        if(dividend == Integer.MIN_VALUE && divisor == -1) {
            return Integer.MAX_VALUE;
        }

        int quotient = 0;
        boolean isNeg = (dividend<0||divisor<0) && !(dividend<0 && divisor<0)? true:false;

        dividend = dividend>0?-dividend:dividend;
        divisor = divisor>0?-divisor:divisor;
        int sum=divisor;

        while(sum>dividend) {
            if(Integer.MIN_VALUE-sum>divisor) {
                quotient++;
                return isNeg? -1*quotient:quotient;
            }
            sum=sum+divisor;
            quotient++;
        }
        if(sum==dividend) {
            quotient++;
        }
        return isNeg? -1*quotient:quotient;
    }

    public static void main(String[] args) {
        System.out.println(divideOptimal(10,3));
    }
}
