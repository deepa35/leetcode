public class ExcelSheetColumn {
    public static int titleToNumberOptimal(String s) {
        int sum = 0;
        if(s==null) {
            return sum;
        }
        for(char c:s.toCharArray()) {
            sum= sum*26 + c - 64;
        }
        return sum;
    }
    public static int titleToNumber(String s) {
        int sum = 0;
        if(s==null) {
            return sum;
        }
        for(int i=0;i<s.length();i++) {
            sum= sum*26 + s.charAt(i) - 64;
        }
        return sum;
    }

    public static void main(String[] args) {
        //System.out.println(10+'A'-6);
        System.out.println(titleToNumber("ZY"));
    }
}
