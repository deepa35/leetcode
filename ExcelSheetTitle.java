public class ExcelSheetTitle {
    public static String convertToTitle(int n) {
        String out = "";
        while(n>0) {
            out = (char) ('A' + --n%26) + out;
            n=n/26;
        }
        return out;
    }

    public static void main(String[] args) {
        System.out.println(convertToTitle(26));
    }
}
