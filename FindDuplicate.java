import java.util.HashSet;
import java.util.Set;

public class FindDuplicate {
    public int findDuplicate(int[] nums) {
        Set<Integer> track = new HashSet<>();
        for(int i=0;i<nums.length;i++) {
            if(track.contains(nums[i])) {
                return nums[i];
            }
            track.add(nums[i]);
        }
        return -1;
    }
}
