import java.util.HashSet;
import java.util.Set;

public class FirstMissingPositives {
    public int firstMissingPositive(int[] nums) {
        if(nums==null || nums.length==0) {
            return 1;
        }
        Set<Integer> a = new HashSet<>();
        int max=Integer.MIN_VALUE;

        for(int num: nums) {
            a.add(num);
            if(num>max) {
                max=num;
            }
        }

        for(int i=1;i<=max;i++) {
            if(!a.contains(i)) {
                return i;
            }
        }
        return max<=0?1:max+1;
    }
}
