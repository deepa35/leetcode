import java.util.HashMap;
import java.util.Map;

public class FirstUniqueCharInStr {
    public static int firstUniqChar(String s) {
        Map<Character,Integer> charFreq = new HashMap<>();
        char[] sArr = s.toCharArray();

        for(int i=0;i<sArr.length;i++) {
            int val = 1;
            if(charFreq.containsKey(sArr[i])) {
                val = charFreq.get(sArr[i]) +1;
            }
            charFreq.put(sArr[i],val);
        }

        for(int i=0;i<sArr.length;i++) {
            if(charFreq.get(sArr[i]) == 1) {
                return i;
            }
        }
        return -1;

    }

    public static void main(String[] args) {
        System.out.println(firstUniqChar("loveleetcode"));
    }
}
