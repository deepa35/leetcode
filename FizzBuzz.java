import java.util.ArrayList;
import java.util.List;

public class FizzBuzz {
    public List<String> fizzBuzz(int n) {
        List<String> out = new ArrayList<>();
        for(int i=1;i<=n;i++) {
            String val = "";
            val=(i%3==0)?val+"Fizz":val;
            val=(i%5==0)?val+"Buzz":val;
            val=(i%3!=0 && i%5!=0)?val+i:val;
            out.add(val);
        }
        return out;
    }
}
