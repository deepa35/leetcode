import java.util.LinkedList;
import java.util.Queue;

public class FriendsCirlcle {
    public int findCircleNum(int[][] M) {
        int[] visited = new int[M.length];
        int count=0;
        Queue<Integer> help=new LinkedList<>();
        for(int i=0;i<M.length;i++) {
            if(visited[i]==0) {
                help.add(i);
                while(!help.isEmpty()) {
                    int s=help.remove();
                    visited[s]=1;
                    for(int j=0;j<M.length;j++) {
                        if(M[s][j]==1 && visited[j]==0) {
                            help.add(j);
                        }
                    }
                }
                count++;
            }
        }
        return count;
    }
}
