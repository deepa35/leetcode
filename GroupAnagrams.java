import java.util.*;

public class GroupAnagrams {
    public static List<List<String>> groupAnagrams(String[] strs) {
        if(strs.length == 0) {
            return new ArrayList<>();
        }

        Map<String,List> tracker = new HashMap<>();

        for(int i =0;i<strs.length;i++) {
            char[] strArr = strs[i].toCharArray();
            Arrays.sort(strArr);
            String str = String.valueOf(strArr);
            List<String> ana = null;
            if(tracker.containsKey(str)) {
               ana = tracker.get(str);

            } else {
                ana = new ArrayList<>();
            }
            ana.add(strs[i]);
            tracker.put(str,ana);
        }
        List<List<String>> out = new ArrayList<>();
        for(String each: tracker.keySet()) {
            out.add(tracker.get(each));
        }
        return out;
    }

    public static void main(String[] args) {
        groupAnagrams(new String[]{"eat", "tea", "tan", "ate", "nat", "bat"});
    }
}
