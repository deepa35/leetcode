public class GuessNumberLowerHigher {
    int mynum=0;
    public int guessNumber(int n) {
        int start = 1, end = n;
        int mid = 0;
        while(start<=end) {
            mid = start+ (end-start)/2;
            int res = guess(mid);
            if(res==0) {
                return mid;
            } else if(res<0) {
                end = mid-1;
            } else {
                start = mid+1;
            }
        }
        return -1;
    }

    private int guess(int a) {
        if(a<mynum){
            return 1;
        } else if (a>mynum) {
            return -1;
        }
        return 0;
    }
}
