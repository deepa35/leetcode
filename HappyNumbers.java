public class HappyNumbers {
    //Square Calculator
    private static int squareCalculator(int number) {
        int square = 0;
        while(number>0) {
            int digit = number%10;
            square = square + (digit * digit);
            number = number/10;
        }
        return square;
    }

    public static boolean isHappy(int n) {
        int slow = n, fast = n;
        do {
            slow = squareCalculator(slow);
            fast = squareCalculator(fast);
            fast = squareCalculator(fast);
        } while(slow!=fast);
        if(slow == 1) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(isHappy(20));
    }
}
