public class HouseRobber {

    public static int rob(int[] nums) {
        if(nums.length == 0) {
            return 0;
        }
        int[] record = new int[nums.length];
        if(nums.length>=1) {
            record[0] = nums[0];
        }

        if(nums.length>=2) {
            record[1] = Math.max(nums[0],nums[1]);
        }

        for(int i = 2;i<nums.length;i++) {
            record[i] = Math.max(nums[i]+record[i-2], record[i-1]);
        }
        return record[nums.length-1];
    }
    
    public static int robOptimal(int[] nums) {
        int prev = 0;
        int prevBeforePrev = 0;

        for(int i=0;i<nums.length;i++) {
            int temp = prev;
            prev = Math.max(prev, prevBeforePrev + nums[i]);
            prevBeforePrev = temp;
        }
        return prev;
    }

    public static void main(String[] args) {
        System.out.println(rob(new int[]{114,117,207,117,235,82,90,67,143,146,53,108,200,91,80,223,58,170,110,236,81,90,222,160,165,195,187,199,114,235,197,187,69,129,64,214,228,78,188,67,205,94,205,169,241,202,144,240}));
    }
}
