public class ImplementationIndexOf {
    public static int strStr(String haystack, String needle) {
        if(needle.equals("")) {
            return 0;
        }
        for(int i=0;i<haystack.length()-needle.length()+1;i++) {
            String elem = haystack.substring(i,i+needle.length());
            if(elem.equals(needle)) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(strStr("aaaaa","bba"));
    }
}
