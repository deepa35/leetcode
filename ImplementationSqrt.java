public class ImplementationSqrt {
    public static int mySqrtOptimal(int x) {
        if(x==0 || x==1) {
            return x;
        }
        long s = x;
        long e = 1;

        while(s>e) {
            s= (s+e)/2;
            e = x/s;
        }
        return (int)s;
    }

    public static int mySqrt(int x) {
        if(x==0 || x==1) {
            return x;
        }
        int answer = 0;
        for(int i=1;i<=x/2;i++) {
            long calcResult = (long)i*i;
            if(calcResult == x) {
                return i;
            }

            if(calcResult > x) {
                return answer;
            }
            answer = i;
        }
        return answer;
    }

    public static void main(String[] args) {
        System.out.println(mySqrtOptimal(2147483647));
    }
}
