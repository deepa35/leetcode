public class IntegerToRoman {
    public static String intToRoman(int num) {
        String[] ones={"","I","II","III","IV","V","VI","VII","VIII","IX"};
        String[] tens={"","X","XX","XXX","XL","L","LX","LXX","LXXX","XC"};
        String[] huns={"","C","CC","CCC","CD","D","DC","DCC","DCCC","CM"};
        String[] thous={"","M","MM","MMM"};

        String out="";
        out=out+thous[num/1000];
        num=num%1000;
        out=out+huns[num/100];
        num=num%100;
        out=out+tens[num/10];
        num=num%10;
        out=out+ones[num];
        return out;
    }

    public static void main(String[] args) {
        System.out.println(intToRoman(28));
    }
}
