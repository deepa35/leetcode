import java.util.HashMap;
import java.util.Map;

public class IsomorphicStrings {

    public static boolean isIsomorphicOptimal(String s, String t) {
        int[] sASCITracker = new int[256];
        int[] tASCITracker = new int[256];

        for(int i=0;i<s.length();i++) {
            if(sASCITracker[s.charAt(i)] != tASCITracker[t.charAt(i)]) {
                return false;
            }
            sASCITracker[s.charAt(i)] = i+1;
            tASCITracker[t.charAt(i)] = i+1;
        }
        return true;
    }

    public static boolean isIsomorphic(String s, String t) {
        if(s.length() != t.length()) {
            return false;
        }

        Map<Character,Character> tracker = new HashMap<>();

        for(int i=0;i<s.length();i++) {
            char sChar = s.charAt(i);
            char tChar = t.charAt(i);

            if(!tracker.containsKey(sChar)) {
                if(!tracker.containsValue(tChar)) {
                    tracker.put(sChar, tChar);
                } else {
                    return false;
                }
            } else {
                if(tracker.get(sChar) != tChar) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(isIsomorphicOptimal("ab","aa"));
    }
}
