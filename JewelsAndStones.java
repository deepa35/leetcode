import java.util.HashSet;

public class JewelsAndStones {

    public static int numJewelsInStones(String J, String S) {
        int count = 0;

        if(J.length() == 0) {
            return count;
        }

        char[] jew = J.toCharArray();
        char[] ston = S.toCharArray();

        HashSet<Character> jewels = new HashSet<>();
        for(int i =0;i<jew.length;i++) {
            jewels.add(jew[i]);
        }

        for(int i=0;i<ston.length;i++) {
            if(jewels.contains(ston[i])) {
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        System.out.println(numJewelsInStones("z","ZZ"));
    }
}
