import java.util.Arrays;
import java.util.Comparator;

public class KClosestPointsToOrigin {
    public int[][] kClosest(int[][] points, int K) {
        int[][] out = new int[K][2];
        Arrays.sort(points, new PointsToOriginComp());
        return Arrays.copyOfRange(points,0,K);
    }
}

class PointsToOriginComp implements Comparator<int []> {
    @Override
    public int compare(int[] o1, int[] o2) {
        double res1 = Math.sqrt(Math.pow(o1[0],2)+Math.pow(o1[1],2));
        double res2 = Math.sqrt(Math.pow(o2[0],2)+Math.pow(o2[1],2));
        return Double.compare(res1,res2);
    }
}
