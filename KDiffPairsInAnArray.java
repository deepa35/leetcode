import java.util.HashMap;
import java.util.Map;

public class KDiffPairsInAnArray {
    public int findPairs(int[] nums, int k) {
        if(nums == null||nums.length==0||k<0) {
            return 0;
        }
        Map<Integer,Integer> count = new HashMap<>();
        int counter=0;
        for(int i=0;i<nums.length;i++) {
            count.put(nums[i],count.getOrDefault(nums[i],0)+1);
        }

        for(Integer each:count.keySet()) {
            if(k==0) {
                if(count.get(each)>=2) {
                    counter++;
                }
            } else {
                if(count.containsKey(each+k)) {
                    counter++;
                }
            }
        }
        return counter;
    }
}
