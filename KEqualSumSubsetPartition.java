public class KEqualSumSubsetPartition {
    public boolean canPartitionKSubsets(int[] nums, int k) {
        int sum=0;
        for(int i=0;i<nums.length;i++) {
            sum+=nums[i];
        }
        if(sum%k!=0) {
            return false;
        }
        boolean[] visited = new boolean[nums.length];
        return helper(nums,visited,sum/k,k,0,0,0);
    }

    private boolean helper(int[] nums,boolean[] visited,int target,int k, int start, int currSum, int currNum) {
        if(k==1) {
            return true;
        }
        if(currSum==target&&currNum>0) {
            return helper(nums,visited,target,k-1,0,0,0);
        }
        for(int i=start;i<nums.length;i++) {
            if(visited[i]==false) {
                visited[i]=true;
                if(helper(nums,visited,target,k,i+1,currSum+nums[i],currNum+1)) {
                    return true;
                }
                visited[i]=false;
            }
        }
        return false;
    }
}
