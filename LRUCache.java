import java.util.LinkedHashMap;

public class LRUCache {
    LinkedHashMap<Integer,Integer> cache;
    int capacity;

    public LRUCache(int capacity) {
        this.capacity=capacity;
        cache=new LinkedHashMap<>(capacity);
    }

    public int get(int key) {
        if(!cache.containsKey(key)) {
            return -1;
        }
        int out = cache.get(key);
        cache.remove(key);
        cache.put(key,out);
        return out;
    }

    public void put(int key, int value) {
        if(cache.containsKey(key)) {
            cache.remove(key);
            cache.put(key,value);
            return;
        }

        if(cache.size()==capacity) {
            cache.remove(cache.keySet().iterator().next());
        }
        cache.put(key,value);
    }
}
