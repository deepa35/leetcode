public class LengthOfLastWord {
    public static int lengthOfLastWord(String s) {
        if(s==null) {
            return 0;
        }
        String[] sSplit = s.trim().split(" ");
        return sSplit[sSplit.length-1].length();
    }

    public static int lengthOfLastWordAlt(String s) {
        if(s==null) {
            return 0;
        }
        String fin = s.trim();
        if(fin.indexOf(" ") == -1) {
            return fin.length();
        }
        return fin.substring(fin.lastIndexOf(" ")+1).length();
    }

    public static void main(String[] args) {
        System.out.println(lengthOfLastWord("      Helloi      world   "));
    }
}
