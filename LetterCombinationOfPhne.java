import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LetterCombinationOfPhne {
    private static List<String> output = new ArrayList<>();
    private static Map<String,String> data = new HashMap<>();

    private static void initialiseData() {
        data.put("2","abc");
        data.put("3","def");
        data.put("4","ghi");
        data.put("5","jkl");
        data.put("6","mno");
        data.put("7","pqrs");
        data.put("8","tuv");
        data.put("9","wxyz");
    }

    public static List<String> letterCombinations(String digits) {
        initialiseData();
        if(digits.length() == 0) {
            return output;
        }
        backTrack("",digits);
        return output;
    }

    private static void backTrack(String combination, String digits) {
        if(digits.length() == 0) {
            output.add(combination);
            return;
        }

        String letters = data.get(digits.substring(0,1));
        for(int i=0;i<letters.length();i++) {
            backTrack(combination+letters.charAt(i),digits.substring(1));
        }
    }

    public static void main(String[] args) {
        List<String> result = letterCombinations("23");
        for(String each:result) {
            System.out.print(each+ ", ");
        }
    }
}
