public class LinkedListCycle {
    public static boolean hasCycle(ListNode head) {
        if(head == null || head.next==null) {
            return false;
        }
        ListNode first = head;
        ListNode second = head.next.next;

        while(first!=null && second!=null && second.next!=null) {
            if(first == second) {
                return true;
            }
            first = first.next;
            second = second.next.next;
        }
        return false;

    }

    public static void main(String[] args) {
        ListNode head = new ListNode(3);
        head.next = new ListNode(2);
        ListNode cyc = head.next;
        head.next.next = new ListNode(0);
        head.next.next.next = new ListNode(-4);
        head.next.next.next.next = cyc;
        System.out.println(hasCycle(head));
    }
}
