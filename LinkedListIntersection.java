public class LinkedListIntersection {
    public static ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        ListNode A = headA;
        ListNode B = headB;

        while(A!=null || B!=null) {
            if(A==B && A!=null) {
                return A;
            }
            if(A==null) {
                A=headB;
            } else {
                A=A.next;
            }
            if(B==null) {
                B=headA;
            } else {
                B=B.next;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        ListNode common = new ListNode(3);
        //common.next = new ListNode(4);
        //common.next.next = new ListNode(5);
        ListNode A = new ListNode(2);
        A.next = new ListNode(6);
        A.next.next = new ListNode(3);
        //A.next.next.next = common;
        ListNode B = new ListNode(2);
        //B.next = common;
       // B.next = common;
        //B.next.next = new ListNode(1);
        //B.next.next.next = common;
        System.out.println(getIntersectionNode(B,common).val);

    }
}
