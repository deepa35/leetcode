import java.util.Arrays;
import java.util.Comparator;

public class LogFileReorder {
    public String[] reorderLogFiles(String[] logs) {
        Arrays.sort(logs, new LogComparator());
        return logs;
    }
}

class LogComparator implements Comparator<String> {
    @Override
    public int compare(String o1, String o2) {
        String s1 = o1.substring(o1.indexOf(" ")+1);
        String s1ID = o1.substring(0,o1.indexOf(" "));
        String s2 = o2.substring(o2.indexOf(" ")+1);
        String s2ID = o2.substring(0,o2.indexOf(" ")+1);

        if(!Character.isDigit(s1.charAt(0)) && !Character.isDigit(s2.charAt(0))) {
            int compareRes = s1.compareTo(s2);
            if(compareRes == 0) {
                return s1ID.compareTo(s2ID);
            }
            return compareRes;
        }

        return Character.isDigit(s1.charAt(0))? (Character.isDigit(s2.charAt(0))?0:1):-1;
    }
}