public class LongestCommonPrefix {
    public static String longestCommonPrefixBF(String[] strs) {
        String out = "";
        if(strs == null || strs.length==0) {
            return out;
        }
        if(strs.length == 1) {
            return strs[0];
        }
        String first = strs[0];
        String second = strs[1];

        for (int i = 2; i <= strs.length; i++) {
            out = "";
            String temp = first.length()<=second.length()?first:second;
            if(temp.equals(second)) {
                second = first;
                first = temp;
            }
            int index=0;
            while(index<first.length()) {
                if(first.charAt(index) != second.charAt(index)) {
                    break;
                }
                out+=first.charAt(index);
                index++;
            }
            if(out.equals("")) {
                return out;
            }
            first = out;
            if(i!=strs.length) {
                second = strs[i];
            }
        }
        return out;
    }

    public static String longestCommonPrefixOptimal(String[] strs) {
        if(strs.length == 0) {
            return "";
        }
        String prefix = strs[0];
        for(int i=1; i<strs.length;i++) {
            while(strs[i].indexOf(prefix) != 0) {
                prefix = prefix.substring(0, prefix.length() -1);
                if(prefix.equals("")) {
                    return "";
                }
            }
        }
        return prefix;
    }

    public static void main(String[] args) {
        String[] in = new String[] {"flower","flow","flight"};
        System.out.println(longestCommonPrefixOptimal(in));
    }
}
