import java.util.Arrays;

public class LongestIncreasingSubsequence {
    public int lengthOfLIS(int[] nums) {
        if(nums==null||nums.length==0) {
            return 0;
        }

        int j=0;
        int[] count = new int[nums.length];
        Arrays.fill(count,1);
        int max = 1;

        for(int i=1;i<nums.length;i++) {
            while(j<i) {
                if(nums[i]>nums[j]) {
                    count[i] = Math.max(count[i],count[j]+1);
                    if(count[i]>max) {
                        max=count[i];
                    }
                }
                j++;
            }
            j=0;
        }

        return max;
    }

    public int lengthOfLISOptimal(int[] nums) {
        int[] dp = new int[nums.length];
        int len =0;
        for(int num: nums) {
            int i = Arrays.binarySearch(dp,0,len,num);
            if(i<0) {
                i=-(i+1);
            }
            dp[i]=num;
            if(i==len) {
                len++;
            }
        }
        return len;
    }
}
