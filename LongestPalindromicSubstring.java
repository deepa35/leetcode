public class LongestPalindromicSubstring {
    private static int l = 0;
    private static int len = 0;

    public static String longestPalindrome(String s) {
        if(s==null) {
            return "";
        }
        if(s.length()<2) {
            return s;
        }

        for(int i=0;i<s.length();i++) {
            expandCenter(s,i,i);
            expandCenter(s,i,i+1);
        }
        return s.substring(l,l+len);
    }

    private static void expandCenter(String s, int left, int right) {
       while(left>=0 && right<s.length() && s.charAt(left)==s.charAt(right)) {
           left--;
           right++;
       }

       if(right-(left+1) > len) {
           len =right-(left+1);
           l= left+1;
       }
    }

    public static void main(String[] args) {
        System.out.println(longestPalindrome("abcdbbfcba"));
    }
}
