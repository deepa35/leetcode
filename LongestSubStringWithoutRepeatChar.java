import java.util.HashSet;
import java.util.Set;

public class LongestSubStringWithoutRepeatChar {
    public static int lengthOfLongestSubstring(String s) {
        int start = 0, end = 0, ans = 0;
        Set<Character> tracker = new HashSet<>();

        while(start<s.length() && end<s.length()) {
            if(tracker.contains(s.charAt(end))) {
                tracker.remove(s.charAt(start));
                start++;
            } else {
                tracker.add(s.charAt(end));
                end++;
                ans = Math.max(ans,end-start);
            }
        }

        return ans;
    }

    public static void main(String[] args) {
        System.out.println(lengthOfLongestSubstring("pwwkew"));
    }
}
