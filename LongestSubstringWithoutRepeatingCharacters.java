import java.util.HashSet;
import java.util.Set;

public class LongestSubstringWithoutRepeatingCharacters {
	public int lengthOfLongestSubstring(String s) {
		int i = 0;
		int j = 0;
		int longestLength = 0;
		Set<Character> set = new HashSet<>();
		
		while(i< s.length() && j< s.length()) {
			if(set.contains(s.charAt(j))) {
				set.remove(s.charAt(i));
				i++;
			} else {
				set.add(s.charAt(j));
				j++;
				longestLength = Math.max(longestLength, j-i);
			}
		}
		return longestLength;
	}
}
