public class LowestCommonAncestorBST {

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        TreeNode small = p;
        TreeNode large = q;

        if(q.val<p.val) {
            small = q;
            large = p;
        }

        if(small.val<=root.val && root.val <=large.val) {
            return root;
        } else if(small.val<root.val && large.val<root.val) {
            return lowestCommonAncestor(root.left,p,q);
        }
        return lowestCommonAncestor(root.right,p,q);
    }

    public static void main(String[] args) {

    }
}
