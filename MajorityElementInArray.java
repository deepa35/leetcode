import java.util.HashMap;
import java.util.Map;

public class MajorityElementInArray {
    public static int majorityElementOptimal(int[] nums) {
        int count = 0;
        int majElem = 0;

        for(int i=0; i< nums.length;i++) {
            if(count == 0) {
                majElem = nums[i];
            }

            if(majElem == nums[i]) {
                count++;
            } else {
                count--;
            }
        }
        return majElem;
    }

    public static int majorityElement(int[] nums) {
        HashMap<Integer,Integer> holder = new HashMap<>();
        int majFreq = Integer.MIN_VALUE;
        int majElem = 0;

        for(int i=0;i<nums.length;i++) {
            if(holder.containsKey(nums[i])) {
                int val = holder.get(nums[i]);
                holder.put(nums[i], ++val);
            } else {
                holder.put(nums[i],1);
            }
        }

        for(Map.Entry each: holder.entrySet()) {
            if((int)each.getValue() > majFreq) {
                majFreq = (int)each.getValue();
                majElem = (int)each.getKey();
            }
        }

        return majElem;
    }

    public static void main(String[] args) {
        System.out.println(majorityElement(new int[]{2,2,1,1,1,2,2}));
    }
}
