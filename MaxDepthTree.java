public class MaxDepthTree {
    public static int maxDepth(TreeNode root) {
        if(root == null) {
            return 0;
        }
        int distance = 1 + depthHelper(root);
        return distance;
    }

    private static int depthHelper(TreeNode node) {
        if(node.left == null && node.right == null) {
            return 0;
        } else if(node.left == null && node.right != null) {
            return 1+depthHelper(node.right);
        } else if(node.left != null && node.right == null) {
            return 1+depthHelper(node.left);
        } else {
            return 1+Math.max(depthHelper(node.left),depthHelper(node.right));
        }
    }

    public static void main(String[] args) {
        TreeNode node = new TreeNode(2);
        node.left = new TreeNode(23);
        node.right = new TreeNode(89);
        node.right.right = new TreeNode(56);
        System.out.println(maxDepth(node));
    }
}
