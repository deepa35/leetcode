public class MaximumSubarray {
    public static int maxSubArray(int[] nums) {
        if(nums == null || nums.length == 0) {
            return 0;
        }

        int maxNow = nums[0];
        int maxSoFar = nums[0];

        for(int i=1;i<nums.length;i++) {
            maxNow = Math.max(nums[i],maxNow+nums[i]);
            maxSoFar = Math.max(maxSoFar,maxNow);
        }
        return maxSoFar;
    }

    public static void main(String[] args) {
        System.out.println(maxSubArray(new int[]{1,2,-10,2,7}));

    }
}
