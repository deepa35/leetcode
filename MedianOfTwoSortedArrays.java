public class MedianOfTwoSortedArrays {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int[] newArr = new int[nums1.length+nums2.length];
        int i=0,j=0,k=0;
        double ans=0;
        while(i<nums1.length && j<nums2.length) {
            if(nums1[i]<nums2[j]) {
                newArr[k]=nums1[i];
                i++;
            } else {
                newArr[k]=nums2[j];
                j++;
            }
            k++;
        }
        while(i<nums1.length) {
            newArr[k]=nums1[i];
            i++;
            k++;
        }

        while(j<nums2.length) {
            newArr[k]=nums2[j];
            j++;
            k++;
        }

        if(newArr.length%2==0) {
            ans=(newArr[newArr.length/2]+newArr[(newArr.length/2)-1])/2.0;
        } else {
            ans = (double)newArr[newArr.length/2];
        }
        return ans;
    }
}
