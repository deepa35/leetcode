import java.util.PriorityQueue;

public class MergeKSortedLists {
    public ListNode mergeKLists(ListNode[] lists) {
        PriorityQueue<ListNode> queue=new PriorityQueue<>((n1, n2)->Integer.compare(n1.val,n2.val));

        for(ListNode each:lists) {
            if(each!=null) {
                queue.offer(each);
            }
        }

        ListNode head=new ListNode(0);
        ListNode temp=head;

        while(!queue.isEmpty()) {
            temp.next=queue.poll();
            temp=temp.next;
            if(temp.next!=null) {
                queue.offer(temp.next);
            }
        }
        return head.next;
    }
}
