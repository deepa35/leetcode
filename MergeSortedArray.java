import java.util.Arrays;

public class MergeSortedArray {
    public static void mergeOptimal(int[] nums1, int m, int[] nums2, int n) {
        int n1 = m-1, n2 = n-1, l = m+n-1;
        while(n1>=0 && n2>=0) {
            if(nums1[n1]>nums2[n2]) {
                nums1[l] = nums1[n1];
                n1--;
            } else {
                nums1[l] = nums2[n2];
                n2--;
            }
            l--;
        }

        while(n2>=0) {
            nums1[l] = nums2[n2];
            l--;
            n2--;
        }
    }

    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int i=0, j=0;
        int length = m;
        while(i<length && j<n) {
            if(nums1[i] > nums2[j]) {
                for(int k=m+n-1;k>i;k--) {
                    nums1[k] = nums1[k-1];
                }
                nums1[i]=nums2[j];
                length++;
                j++;
            }
            i++;
        }
        while(j<n) {
            nums1[i] = nums2[j];
            i++; j++;
        }
    }

    public static void main(String[] args) {
        int[] nums1 = new int[]{2};
        int[] nums2 = new int[]{0};
        merge(nums1,1,nums2,0);
        System.out.println(Arrays.toString(nums1));
    }
}
