public class MergeTwoSortedList {
    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode out = new ListNode(0);
        ListNode builder = out;
        ListNode l1Dupe = l1;
        ListNode l2Dupe = l2;

        while(l1Dupe!= null && l2Dupe!=null) {
            if(l1Dupe.val <= l2Dupe.val) {
                builder.next = new ListNode(l1Dupe.val);
                l1Dupe = l1Dupe.next;
            } else {
                builder.next = new ListNode(l2Dupe.val);
                l2Dupe = l2Dupe.next;
            }
            builder = builder.next;
        }
        if(l1Dupe != null || l2Dupe!=null) {
            while(l1Dupe != null) {
                builder.next = new ListNode(l1Dupe.val);
                l1Dupe = l1Dupe.next;
                builder = builder.next;
            }
            while(l2Dupe != null) {
                builder.next = new ListNode(l2Dupe.val);
                l2Dupe = l2Dupe.next;
                builder = builder.next;
            }
        }
        return out.next;
    }
    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        l1.next = new ListNode(2);
        (l1.next).next = new ListNode(4);
        ListNode l2 = new ListNode(1);
        l2.next = new ListNode(3);
        (l2.next).next = new ListNode(4);

        ListNode out = mergeTwoLists(l1,l2);
        while(out!=null) {
            System.out.print(out.val+",");
            out = out.next;
        }
    }
}

class ListNode {
      int val;
      ListNode next;
      ListNode(int x) { val = x; }
 }
