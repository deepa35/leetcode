import java.util.ArrayList;
import java.util.Stack;

public class MinStack {
    /** initialize your data structure here. */
    private Stack<Integer> stack;
    private int minVal;

    public MinStack() {
        stack = new Stack<>();
        minVal = Integer.MAX_VALUE;
    }

    public void push(int x) {
        if(x<=minVal) {
            stack.push(minVal);
            minVal = x;
        }
        stack.push(x);
    }

    public void pop() {
        if(stack.pop() == minVal ) {
            minVal = stack.pop();
        }
    }

    public int top() {
        return stack.peek();
    }

    public int getMin() {
        return minVal;
    }

    //Using ArrayList
    /*
    private ArrayList<Integer> stack;
    private int minVal;

    public MinStack() {
        stack = new ArrayList<>();
        minVal = Integer.MAX_VALUE;
    }

    public void push(int x) {
        if(x<=minVal) {
            stack.add(minVal);
            minVal = x;
        }
        stack.add(x);
    }

    public void pop() {
        if(stack.remove(stack.size()-1) == minVal ) {
            minVal = stack.remove(stack.size()-1);
        }
    }

    public int top() {
        return stack.get(stack.size()-1);
    }

    public int getMin() {
        return minVal;
    }
     */
}
