import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MostCommonWord {
    public static String mostCommonWord(String paragraph, String[] banned) {
        Set<String> ban = new HashSet<>();
        String mFW = "";
        int maxCount = 0;
        for(int i=0;i<banned.length;i++) {
            ban.add(banned[i]);
        }

        Map<String,Integer> wordCount = new HashMap<>();

        paragraph = paragraph.replaceAll("[^a-zA-Z ]"," ").toLowerCase();
        String[] para = paragraph.split("\\s+");
        for(String each:para) {
            if(!ban.contains(each)) {
                if (wordCount.containsKey(each)) {
                    int count = wordCount.get(each);
                    wordCount.put(each, ++count);
                } else {
                    wordCount.put(each, 1);
                }

                if(wordCount.get(each)>maxCount) {
                    maxCount=wordCount.get(each);
                    mFW=each;
                }
            }
        }

        return mFW;
    }

    public static void main(String[] args) {
        System.out.println(mostCommonWord("a, a, a, a, b,b,b,c, c", new String[]{"a"}));
    }
}
