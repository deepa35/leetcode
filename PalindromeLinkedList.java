public class PalindromeLinkedList {
    //O(n) space complexity
    public static boolean isPalindrome(ListNode head) {
        if(head == null) {
            return true;
        }

        ListNode reverse = new ListNode(head.val);
        ListNode correct = head.next;

        while(correct != null) {
            ListNode a = new ListNode(correct.val);
            a.next = reverse;
            reverse = a;
            correct = correct.next;
        }
        correct = head;

        while(correct!= null && reverse !=null) {
            if(correct.val != reverse.val) {
                return false;
            }
            correct = correct.next;
            reverse = reverse.next;
        }

        if(correct!= null || reverse != null) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        ListNode a = new ListNode(1);
        a.next = new ListNode(2);
        a.next.next = new ListNode(1);
        System.out.println(isPalindrome(a));
    }
}
