import java.util.Stack;

public class PalindromeNumber {
    public static boolean isPalindrome(int x) {
        String num = Integer.toString(x);
        return isPalindromeHelper(num);

    }

    //Optimal
    private static boolean isPalindromeHelper(String num) {
        if(num.length() <= 1) {
            return true;
        }
        int start = 0;
        int end = num.length()-1;
        if(num.charAt(start) == num.charAt(end)) {
            return isPalindromeHelper(num.substring(1,num.length()-1));
        }
        return false;

    }

    public static void main(String[] args) {
        System.out.println(isPalindrome(121));
    }
}
