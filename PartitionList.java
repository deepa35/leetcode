public class PartitionList {
    public static ListNode partition(ListNode head, int x) {
        ListNode before = new ListNode(0);
        ListNode result = before;
        ListNode after = new ListNode(0);
        ListNode joint = after;

        while(head!=null) {
            if(head.val <x) {
                before.next = head;
                before = before.next;
            } else {
                after.next = head;
                after = after.next;
            }
            head = head.next;
        }
        after.next = null;
        before.next = joint.next;
        return result.next;
    }

    public static void main(String[] args) {
        ListNode a = new ListNode(1);
        a.next = new ListNode(4);
        a.next.next = new ListNode(3);
        a.next.next.next = new ListNode(2);
        a.next.next.next.next = new ListNode(5);
        a.next.next.next.next.next = new ListNode(2);

        partition(a,3);

    }

}
