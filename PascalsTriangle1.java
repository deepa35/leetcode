import java.util.ArrayList;
import java.util.List;

public class PascalsTriangle1 {
    public static List<List<Integer>> generate(int numRows) {
        List<List<Integer>> out = new ArrayList();
        if(numRows==0) {
            return out;
        }
        out.add(new ArrayList<>());
        out.get(0).add(1);

        for(int i=2;i<=numRows;i++) {
            if(out.size() < i) {
                out.add(new ArrayList<>());
            }
            out.get(i-1).add(1);
            ArrayList<Integer> prev = (ArrayList<Integer>) out.get(i-2);
            for(int j=0;j<prev.size();j++) {
                if(j==prev.size()-1) {
                    out.get(i-1).add(prev.get(j));
                } else {
                    out.get(i-1).add(prev.get(j) + prev.get(j+1));
                }
            }
        }
        return out;
    }

    public static void main(String[] args) {
        List<List<Integer>> out = generate(5);
        System.out.println(out);
    }
}
