import java.util.ArrayList;
import java.util.List;

public class PascalsTriangle2 {
    public static List<Integer> getRowOptimal(int rowIndex) {
        List<Integer> out = new ArrayList<>();
        if(rowIndex < 0) {
            return out;
        }

        long count = 1;
        out.add((int)count);
        for(int i=0;i<rowIndex;i++) {
            count = count * (rowIndex-i) /(i+1);
            out.add((int)count);
        }
        return out;
    }

    public static List<Integer> getRow(int rowIndex) {
        List<Integer> out = new ArrayList<>();
        if(rowIndex < 0) {
            return out;
        }
        for(int i=0;i<rowIndex+1;i++) {
            out.add(0,1);
            for(int j=1;j<out.size()-1;j++) {
                out.set(j,out.get(j)+out.get(j+1));
            }
        }
        return out;
    }
    public static void main(String[] args) {
        System.out.println(getRow(5));
    }
}
