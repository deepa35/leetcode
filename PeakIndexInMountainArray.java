public class PeakIndexInMountainArray {

    public int peakIndexInMountainArrayOptimal(int[] A) {
        int low=0, high=A.length-1;
        while(low<high) {
            int mid=(A[low]+A[high])/2;
            if(A[mid]<A[mid+1]) {
                low = mid+1;
            } else {
                high=mid;
            }
        }
        return low;
    }

    //O(n)
    public int peakIndexInMountainArray(int[] A) {
        for(int i=1;i<A.length-1;i++) {
            if(A[i-1]<A[i] && A[i]>A[i+1]) {
                return i;
            }
        }
        return -1;
    }
}
