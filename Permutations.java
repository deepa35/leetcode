import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Permutations {
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> out = new ArrayList<>();
        List<Integer> nList = new ArrayList<>();

        for(int num:nums) {
            nList.add(num);
        }

        backtrack(out,nList,nums.length,0);
        return out;
    }

    private void backtrack(List<List<Integer>> out,List<Integer> nums,int len,int first) {
        if(first==len) {
            out.add(new ArrayList<>(nums));
        }

        for(int i=first;i<len;i++) {
            Collections.swap(nums,first,i);
            backtrack(out,nums,len,first+1);
            Collections.swap(nums,first,i);
        }
    }
}
