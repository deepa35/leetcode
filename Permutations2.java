import java.util.*;

public class Permutations2 {
    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> out = new ArrayList<>();
        List<Integer> nList = new ArrayList<>();
        for(int num:nums) {
            nList.add(num);
        }
        backtrack(out,nList,nums.length,0);
        return out;
    }

    private void backtrack(List<List<Integer>> out,List<Integer> nums,int len,int first) {
        if(first==len) {
            out.add(new ArrayList<>(nums));
        }

        Set<Integer> track=new HashSet<>();
        for(int i=first;i<len;i++) {
            if(track.add(nums.get(i))) {
                Collections.swap(nums,first,i);
                backtrack(out,nums,len,first+1);
                Collections.swap(nums,first,i);
            }
        }
    }
}
