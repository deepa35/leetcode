import java.util.Arrays;

public class PlusOne {
    public static int[] plusOne(int[] digits) {
        if(digits == null) {
            return null;
        }
        if(digits[digits.length-1] != 9) {
            digits[digits.length-1]++;
            return digits;
        }

        for(int i=digits.length-1; i>=0;i--) {
            if(digits[i] == 9) {
                digits[i] = 0;
            } else {
                digits[i]++;
                return digits;
            }
        }

        int[] digitsNew = new int[digits.length+1];
        digitsNew[0] = 1;
        return digitsNew;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(plusOne(new int[]{9,5,9})));
    }
}
