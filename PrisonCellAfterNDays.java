import java.util.Arrays;

public class PrisonCellAfterNDays {

    //Optimal solution
    public static int[] prisonAfterNDaysOptimal(int[] cells, int N) {
        int[] newCell = new int[8];
        for(int i=1;i<7;i++) {
            newCell[i] = (cells[i-1]==cells[i+1])?1:0;
        }
        cells = newCell;
        int[] firstRow = newCell;
        int cycleDetector = 1;
        --N;
        while(N>0) {
            newCell = new int[8];
            for(int i=1;i<7;i++) {
                newCell[i] = (cells[i-1]==cells[i+1])?1:0;
            }
            cells = newCell;
            N--;
            if(Arrays.equals(newCell,firstRow)) {
                N=N%cycleDetector;
            }
            cycleDetector++;
        }
        return cells;
    }

    //Brute Force
    public static int[] prisonAfterNDays(int[] cells, int N) {
        int[] newCell;
        for(int i=1;i<=N;i++) {
            newCell = new int[8];
            for(int j=1;j<7;j++) {
                if(cells[j-1]==cells[j+1]) {
                    newCell[j] = 1;
                } else {
                    newCell[j] = 0;
                }
            }
            cells = newCell;
        }
        return cells;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(prisonAfterNDaysOptimal(new int[]{1,0,0,1,0,0,1,0},1000000000)));
    }
}

