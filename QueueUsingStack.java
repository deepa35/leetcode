import java.util.Stack;

public class QueueUsingStack {
    private Stack<Integer> queue;
    private Stack<Integer> helper;
    int front;

    /** Initialize your data structure here. */
    public QueueUsingStack() {
        queue = new Stack<>();
        helper = new Stack<>();
        front = 0;
    }

    /** Push element x to the back of queue. */
    public void push(int x) {
        if(queue.isEmpty()) {
            front = x;
        }
        queue.push(x);
    }

    /** Removes the element from in front of queue and returns that element. */
    public int pop() {
        while(!queue.isEmpty()) {
            int elem = queue.pop();
            helper.push(elem);
        }
        int ret= helper.pop();
        if(!helper.isEmpty()){
            front = helper.peek();
        }

        while(!helper.isEmpty()) {
            queue.push(helper.pop());
        }
        return ret;
    }

    /** Get the front element. */
    public int peek() {
        return front;
    }

    /** Returns whether the queue is empty. */
    public boolean empty() {
        return queue.isEmpty();
    }
}
