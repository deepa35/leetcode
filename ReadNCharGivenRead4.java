public class ReadNCharGivenRead4 {
    public int read(char[] buf, int n) {
        int index = 0;
        char[] holder = new char[4];

        while(index>=0) {
            int read = read4(holder);
            read = Math.min(read, n-index);
            for(int i=0;i<read;i++) {
                buf[index] = holder[i];
                index++;
            }
            if(read<4) {
                break;
            }
        }
        return index;
    }

    private int read4(char[] buf) {
        return 4;
    }
}
