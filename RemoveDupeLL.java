
public class RemoveDupeLL {
    public static ListNode deleteDuplicates(ListNode head) {
        ListNode h = head;
        ListNode prev = null;
        ListNode curr = null;
        while(h!= null && h.next != null) {
            prev = h;
            curr = h.next;
            if(prev.val == curr.val) {
                prev.next = curr.next;
            } else {
                h = h.next;
            }
        }
        return head;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(1);
        (head.next).next = new ListNode(1);
        //((head.next).next).next = new ListNode(3);
        //(((head.next).next).next).next = new ListNode(3);
        ListNode out = deleteDuplicates(head);
        while(out!=null) {
            System.out.print(out.val+",");
            out = out.next;
        }
    }
}

