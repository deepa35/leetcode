public class RemoveDupesSortedArray {
    public static int removeDuplicates(int[] nums) {
        if(nums == null || nums.length == 0) {
            return 0;
        }
        int index = 1;
        int ref = nums[0];
        for(int i=1;i<nums.length;i++) {
            if(ref != nums[i]) {
                nums[index] = nums[i];
                index++;
            }
            ref = nums[i];
        }
        return index;
    }

    public static void main(String[] args) {
        System.out.println(removeDuplicates(new int[]{1,1,2}));

    }
}
