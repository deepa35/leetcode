public class RemoveLinkedListElement {
    public static ListNode removeElements(ListNode head, int val) {
       ListNode prev = null;

       while(head != null && head.val == val) {
           head = head.next;
       }

       ListNode current = head;
       while(current != null) {
           if(current.val == val) {
               prev.next = current.next;
           } else {
               prev = current;
           }
           current = current.next;
       }

       return head;
    }

    public static void main(String[] args) {
        ListNode a = new ListNode(1);
        a.next = new ListNode(2);
        a.next.next = new ListNode(2);
        a.next.next.next = new ListNode(1);
        System.out.println(removeElements(a,2).val);
    }
}
