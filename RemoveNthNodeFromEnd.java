public class RemoveNthNodeFromEnd {
    public static ListNode removeNthFromEnd(ListNode head, int n) {
        int numNodes = 0;
        ListNode a = head;

        while(a!=null) {
            numNodes++;
            a=a.next;
        }

        int index = numNodes-n-1;
        a=head;

        if(index == -1) {
            head = head.next;
            return head;
        }

        while(index>0) {
            index--;
            a=a.next;
        }

        if(a.next != null) {
            a.next = a.next.next;
        } else {
            a.next = null;
        }
        return head;
    }

    public static void main(String[] args) {
        removeNthFromEnd(new ListNode(1),1);
    }
}
