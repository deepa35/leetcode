public class ReverseBinary {
    public static int reverseBits(int n) {
        int reverse = 0;
        for(int i=0;i<32;i++) {
            reverse <<= 1;
            if((int)(n&1) == 1) {
                reverse ^= 1;
            }
            n>>=1;
        }
        return reverse;
    }
    public static void main(String[] args) {
        System.out.println(reverseBits(43261596));
    }
}
