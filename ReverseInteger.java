import java.util.ArrayList;
import java.util.List;

public class ReverseInteger {

    //First - BF logic
    public static int reverseBF(int x) {
        long out = 0;
        boolean neg = x<0 ? true:false;
        List<Integer> outList = new ArrayList<>();
        long quo = x<0 ? -(long)x:(long)x;

        while(quo != 0) {
            outList.add((int)(quo%10));
            quo = quo/10;
        }

        for(int i=0; i< outList.size(); i++) {
            out = out + outList.get(i) * (long) Math.pow(10,outList.size()-i-1);
        }

        if(out>=2147483647) {
            return 0;
        }
        return neg?-(int)out:(int)out;
    }

    //Optimal solution
    public static int reverseOptimal(int x) {
        int out = 0;
        while(x != 0) {
            int digit =x%10;
            x = x/10;
            if(out>Integer.MAX_VALUE/10 || (out==Integer.MAX_VALUE/10 && digit > 7)) {
                return 0;
            }
            if(out<Integer.MIN_VALUE/10 || (out==Integer.MIN_VALUE/10 && digit < -8)) {
                return 0;
            }
            out = out * 10 + digit;
        }
        return out;
    }

    public static void main(String[] args) {
        System.out.println(reverseOptimal(-21474836));
    }
}
