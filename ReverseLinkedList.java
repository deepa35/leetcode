public class ReverseLinkedList {
    public static ListNode reverseList(ListNode head) {
        ListNode prev = null;
        ListNode current = head;
        ListNode temp = null;

        while(current != null) {
            temp = current.next;
            current.next = prev;
            prev=current;
            current = temp;
        }

        return prev;
    }

    public static void main(String[] args) {
        ListNode a = new ListNode(1);
        a.next = new ListNode(2);
        /*a.next.next = new ListNode(3);
        a.next.next.next = new ListNode(4);
        a.next.next.next.next = new ListNode(5);*/

        ListNode b = reverseList(null);
        while(b!=null) {
            System.out.print(b.val + "->");
            b=b.next;
        }
        System.out.print("NULL");

    }
}
