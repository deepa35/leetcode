public class ReverseNodesInKGroups {
    public ListNode reverseKGroup(ListNode head, int k) {
        if(head==null || k<=1) {
            return head;
        }
        ListNode temp = new ListNode(0);
        temp.next=head;
        ListNode prev=temp;
        ListNode curr=head;
        ListNode next=null;
        int iters = 0;
        for(ListNode i=head;i!=null;i=i.next) {
            iters++;
        }
        iters=iters/k;
        for(int i=0;i<iters;i++) {
            for(int j=0;j<k-1;j++) {
                next=curr.next;
                curr.next=next.next;
                next.next=prev.next;
                prev.next=next;
            }
            prev=curr;
            curr=prev.next;
        }
        return temp.next;
    }
}
