public class ReverseStringInAWord {
    public static String reverseWords(String s) {
        if(s==null || s.length()<2) {
            return s;
        }
        String out = "";
        int start = 0, end = 0;

        while(start>=0 && start<s.length()) {
            end=s.indexOf(" ",start);
            if(end==-1) {
                end=s.length();
            }
            String word = s.substring(start,end);
            for(int i=word.length()-1;i>=0;i--) {
                out = out+word.charAt(i);
            }
            out+=" ";
            start = end+1;
        }
        return out.trim();
    }

    public static void main(String[] args) {
        System.out.println(reverseWords("Let's take LeetCode contest"));
    }

}