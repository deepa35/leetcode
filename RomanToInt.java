public class RomanToInt {
    public static int romanToInt(String s) {
        int out = 0;
        return romanToIntHelper(s,out);
    }

    private static int romanToIntHelper(String s, int out) {
        if(s.length() == 0) {
            return out;
        }
        if(s.charAt(0) == 'I') {
            if(s.length()<2) {
                return romanToIntHelper(s.substring(1),out+1);
            } else {
                if(s.charAt(1) == 'V') {
                    return romanToIntHelper(s.substring(2),out+4);
                } else if(s.charAt(1) == 'X') {
                    return romanToIntHelper(s.substring(2),out+9);
                } else{
                    return romanToIntHelper(s.substring(1),out+1);
                }
            }
        } else if(s.charAt(0) == 'V') {
            return romanToIntHelper(s.substring(1),out+5);
        } else if(s.charAt(0) == 'X') {
            if(s.length()<2) {
                return romanToIntHelper(s.substring(1),out+10);
            } else {
                if(s.charAt(1) == 'L') {
                    return romanToIntHelper(s.substring(2),out+40);
                } else if(s.charAt(1) == 'C') {
                    return romanToIntHelper(s.substring(2),out+90);
                } else{
                    return romanToIntHelper(s.substring(1),out+10);
                }
            }
        } else if(s.charAt(0) == 'L') {
            return romanToIntHelper(s.substring(1),out+50);
        } else if(s.charAt(0) == 'C') {
            if(s.length()<2) {
                return romanToIntHelper(s.substring(1),out+100);
            } else {
                if(s.charAt(1) == 'D') {
                    return romanToIntHelper(s.substring(2),out+400);
                } else if(s.charAt(1) == 'M') {
                    return romanToIntHelper(s.substring(2),out+900);
                } else{
                    return romanToIntHelper(s.substring(1),out+100);
                }
            }
        } else if(s.charAt(0) == 'D') {
            return romanToIntHelper(s.substring(1),out+500);
        } else if(s.charAt(0) == 'M') {
            return romanToIntHelper(s.substring(1),out+1000);
        }
        return -1;

    }

    public static void main(String[] args) {
        System.out.println(romanToInt("MCMXCIV"));
    }
}
