import java.util.Arrays;

public class RotateImage {
    public static void rotate(int[][] matrix) {
        for(int r=0;r<matrix.length;r++) {
            for(int c=r; c<matrix[0].length;c++) {
                if(r!=c) {
                    int temp = matrix[r][c];
                    matrix[r][c] = matrix[c][r];
                    matrix[c][r] = temp;
                }
            }
        }

        for(int r=0;r<matrix.length;r++) {
            for(int c=0;c<matrix[0].length/2;c++) {
                if(c!=matrix[0].length-c-1) {
                    int temp = matrix[r][c];
                    matrix[r][c] = matrix[r][matrix[0].length - c - 1];
                    matrix[r][matrix[0].length - c - 1] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[][] matrix = {{5, 1, 9,11},{2, 4, 8,10},{13, 3, 6, 7},{15,14,12,16}};
        rotate(matrix);
        for(int[] each:matrix) {
            System.out.println(Arrays.toString(each));
        }
    }
}
