public class SearchInRotatedSortedArray {
    public static int search(int[] nums, int target) {
        if(nums.length == 0 || (nums.length==1 && nums[0]!=target)) {
            return -1;
        }

        if(nums[0]==target) {
            return 0;
        } else if(nums[nums.length-1]==target) {
            return nums.length-1;
        }

        int startIndex = 0;
        int newStart=0, newEnd=0;
        int start = 0, end=nums.length-1, mid = 0;
        if(nums[start]<nums[end]) {
            startIndex=0;
        } else {
            while(start<=end) {
                mid = start+(end-start)/2;
                if(nums[mid+1]<nums[mid]) {
                    startIndex = mid+1;
                    break;
                }

                if(nums[start] > nums[mid]) {
                    end=mid-1;
                } else {
                    start = mid+1;
                }
            }
        }

        if(nums[startIndex]==target) {
            return startIndex;
        }

        if(startIndex==0) {
            newStart = startIndex++;
            newEnd = nums.length-1;
        } else if (startIndex == nums.length-1) {
            newStart = 0;
            newEnd = startIndex-1;
        } else {
            if(target>nums[0]) {
                newStart=0;
                newEnd=startIndex-1;
            } else  {
                newStart=startIndex+1;
                newEnd = nums.length-1;
            }
        }

        while(newStart<=newEnd) {
            int m=newStart+(newEnd-newStart)/2;
            if(nums[m]==target) {
                return m;
            } else if(nums[m]<target) {
                newStart = m+1;
            } else {
                newEnd=m-1;
            }
        }
        return -1;
    }


    public static void main(String[] args) {
        System.out.println(search(new int[]{8,9,2,3,4},9));
    }
}
