public class SearchInsert {
    public static int searchInsert(int[] nums, int target) {
        if(nums == null || nums.length == 0 || nums[0]>target) {
            return 0;
        }
        if(target>nums[nums.length-1]) {
            return nums.length;
        }
        return binarySearch(nums,target,0,nums.length);
    }

    private static int binarySearch(int[] nums, int target, int start, int end) {
        if(end<start) {
            return start;
        }
        int mid = (start+end)/2;
        if(nums[mid] == target) {
            return mid;
        } else if(target > nums[mid]) {
            return binarySearch(nums,target,mid+1,end);
        } else {
            return binarySearch(nums,target,start,mid-1);
        }
    }

    public static void main(String[] args) {
        System.out.println(searchInsert(new int[]{1,3},2));
    }
}
