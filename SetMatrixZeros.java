import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SetMatrixZeros {

    public static void setZeroesOptimal(int[][] matrix) {
        boolean isCol= false;

        for(int r=0;r<matrix.length;r++) {
            if(matrix[r][0] == 0) {
                isCol = true;
            }
            for(int c=1;c<matrix[0].length;c++) {
                if(matrix[r][c]==0) {
                    matrix[r][0] = 0;
                    matrix[0][c] = 0;
                }
            }
        }

        for(int r=1;r<matrix.length;r++) {
            for(int c=1;c<matrix[0].length;c++) {
                if(matrix[r][0]==0 || matrix[0][c]==0) {
                    matrix[r][c] = 0;
                }
            }
        }

        if(matrix[0][0] == 0) {
            for(int i=0;i<matrix[0].length;i++) {
                matrix[0][i] = 0;
            }
        }

        if(isCol) {
            for(int i=0;i<matrix.length;i++) {
                matrix[i][0] = 0;
            }
        }
    }

    public static void setZeroes(int[][] matrix) {
        Set<Integer> row = new HashSet<>();
        Set<Integer> col = new HashSet<>();

        for(int r=0;r<matrix.length;r++) {
            for(int c=0;c<matrix[0].length;c++) {
                if(matrix[r][c] == 0) {
                    row.add(r);
                    col.add(c);
                }
            }
        }
        for(int r=0;r<matrix.length;r++) {
            for (int c = 0; c < matrix[0].length; c++) {
                if(row.contains(r) || col.contains(c)) {
                    matrix[r][c] = 0;
                }
            }
        }
    }
    public static void main(String[] args) {
        int[][] nums = {{1,1,1},{1,0,1},{1,1,1}};
        setZeroesOptimal(nums);
        for(int[] each: nums) {
            System.out.println(Arrays.toString(each));
        }

    }
}
