public class ShortestWordDistance {
    public static int shortestDistance(String[] words, String word1, String word2) {
        int shDist = words.length;
        int w1Index = -1;
        int w2Index = -1;

        for(int i=0;i<words.length;i++) {
            if(words[i].equals(word1)){
                w1Index = i;
            } else if(words[i].equals(word2)) {
                w2Index = i;
            }

            if(w1Index!=-1 && w2Index!=-1) {
                shDist = Math.min(shDist,Math.abs(w1Index-w2Index));
            }
        }
        return shDist;
    }

    public static void main(String[] args) {
        System.out.println(shortestDistance(new String[]{"practice", "makes", "perfect", "coding", "makes"},"practice","coding"));
    }
}
