public class SingleNumber {
    public static int singleNumber(int[] nums) {
        if(nums.length < 1) {
            return 0;
        }
        int xorSum = nums[0];

        for(int i=1;i<nums.length;i++) {
            xorSum = xorSum ^ nums[i];
        }
        return xorSum;
    }

    public static void main(String[] args) {
        System.out.println(singleNumber(new int[]{4,1,2,1,2}));
    }
}
