import java.util.Arrays;

public class SortArrayByParity {
    public static int[] sortArrayByParity(int[] A) {
        int s=0, e=A.length-1;
        while(s<e) {
            if(A[s]%2>A[e]%2) {
                int temp=A[s];
                A[s]=A[e];
                A[e]=temp;
            }
            if(A[s]%2==0) {
                s++;
            }
            if(A[e]%2==1) {
                e--;
            }
        }
        return A;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(sortArrayByParity(new int[]{3,1,2,4})));
    }

}
