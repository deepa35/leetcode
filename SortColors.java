public class SortColors {
    public static void sortColors(int[] nums) {
        if(nums==null||nums.length==0) {
            return;
        }

        int[] color = new int[3];
        for(int i=0;i<nums.length;i++) {
            color[nums[i]]++;
        }

        int index=0;
        for(int i=0;i<3;i++) {
            int count = color[i];
            while(count>0) {
                nums[index]=i;
                index++;
                count--;
            }
        }
    }

    public static void main(String[] args) {
        sortColors(new int[]{2,0,2,1,1,0});
    }
}
