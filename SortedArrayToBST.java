public class SortedArrayToBST {
    public static TreeNode sortedArrayToBST(int[] nums) {
        return sortedArrayToBSTHelper(nums,0, nums.length-1);
    }

    private static TreeNode sortedArrayToBSTHelper( int[]nums, int start,int end) {
        if(start>end) {
            return null;
        }

        int mid = (start+end)/2;
        TreeNode root = new TreeNode(nums[mid]);
        root.left = sortedArrayToBSTHelper(nums,start, mid-1);
        root.right = sortedArrayToBSTHelper(nums,mid+1, end);
        return root;
    }

    public static void main(String[] args) {
        System.out.println(sortedArrayToBST(new int[]{-10,-3,0,5,9}));
    }
}
