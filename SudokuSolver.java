public class SudokuSolver {
    public void solveSudoku(char[][] board) {
        solver(board);
    }
    private boolean solver(char[][] board) {
        for(int i=0;i<board.length;i++) {
            for(int j=0;j<board[0].length;j++) {
                if(board[i][j]=='.') {
                    for(char c='1';c<='9';c++) {
                        if(isValid(board,i,j,c)) {
                            board[i][j]=c;
                            if(solver(board)) {
                                return true;
                            }
                            board[i][j]='.';
                        }
                    }
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isValid(char[][] board, int r, int c, char num) {
        for(int i=0;i<9;i++) {
            if(board[i][c]!='.' && board[i][c]==num) {
                return false;
            }
            if(board[r][i]!='.' && board[r][i]==num) {
                return false;
            }
            if(board[3*(r/3)+i/3][3*(c/3)+i%3]!='.' && board[3*(r/3)+i/3][3*(c/3)+i%3]==num) {
                return false;
            }
        }
        return true;
    }
}
