public class SwapNodesInPairs {
    public ListNode swapPairs(ListNode head) {
        ListNode temp=new ListNode(0);
        ListNode tempTracker = temp;
        temp.next = head;
        ListNode first=head;
        ListNode second= null;
        if(head!=null) {
            second=head.next;
        }

        while(first!=null && second!=null) {
            temp.next=second;
            ListNode t=second.next;
            second.next=first;
            first.next=t;

            temp=first;
            first=t;
            if(t!=null) {
                second=t.next;
            }
        }

        return tempTracker.next;

    }
}
