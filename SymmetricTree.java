public class SymmetricTree {
    public static boolean isSymmetric(TreeNode root) {
        return isSymmHelper(root, root);
    }

    private static boolean isSymmHelper(TreeNode left, TreeNode right) {
        if(left==null && right == null) {
            return true;
        }
        if(left==null || right==null) {
            return false;
        }
        return left.val == right.val && isSymmHelper(left.left, right.right) && isSymmHelper(left.right,right.left);
    }

    public static void main(String[] args) {

    }
}
