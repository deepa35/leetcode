import java.util.Arrays;

public class ThreeSumClosest {
    public static int threeSumClosest(int[] nums, int target) {
        int sumTotal = nums[0]+nums[1]+nums[nums.length-1];
        Arrays.sort(nums);
        for(int i=0;i<nums.length-2;i++) {
            int low = i+1;
            int high = nums.length-1;
            while(low<high) {
                int sumNow= nums[i] + nums[low]+ nums[high];
                if(sumNow<target) {
                    low++;
                } else{
                    high--;
                }
                if(Math.abs(target-sumNow)<Math.abs(target-sumTotal)) {
                    sumTotal=sumNow;
                }
            }
        }
        return sumTotal;
    }

    public static void main(String[] args) {
        System.out.println(threeSumClosest(new int[]{-1, 2, 1, -4},-1));
    }
}
