import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThreeSums {
    public static List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> ans = new ArrayList<>();
        for(int i=0;i<nums.length-2;i++) {
            if(i==0 || nums[i]!=nums[i-1]) {
                int low= i+1;
                int high=nums.length-1;
                int total = nums[i];
                while(low<high) {
                    if(total+nums[low]+nums[high] == 0) {
                        ans.add(Arrays.asList(nums[i],nums[low],nums[high]));
                        while(low<high && nums[low]==nums[low+1]) {
                            low++;
                        }
                        while(low<high && nums[high]==nums[high-1]) {
                            high--;
                        }
                        low++;
                        high--;
                    } else if(total+nums[low]+nums[high] < 0) {
                        low++;
                    } else {
                        high--;
                    }
                }
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        System.out.println(threeSum(new int[]{-1, 0, 1, 2, -1, -4}));
    }
}
