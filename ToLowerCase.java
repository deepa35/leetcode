public class ToLowerCase {
    public String toLowerCase(String str) {
        String out = "";
        for(int i=0;i<str.length();i++) {
            if(Character.isUpperCase(str.charAt(i))) {
                out+=Character.toLowerCase(str.charAt(i));
            } else {
                out+=str.charAt(i);
            }
        }
        return out;

    }
}
