import java.util.*;

public class TopKFrequentWords {
    public List<String> topKFrequent(String[] words, int k) {
        Map<String,Integer> myMap = new HashMap<>();

        for(int i=0;i<words.length;i++) {
            myMap.put(words[i],myMap.getOrDefault(words[i],0)+1);
        }
        /*List<String> out = new ArrayList<>(myMap.keySet());
        Collections.sort(out,(w1,w2)->myMap.get(w1)==myMap.get(w2)?w1.compareTo(w2):Integer.compare(myMap.get(w2),myMap.get(w1)));

        return out.subList(0,k); */
        PriorityQueue<String> help = new PriorityQueue<>((w1, w2)->myMap.get(w1)==myMap.get(w2)? w2.compareTo(w1):Integer.compare(myMap.get(w1),myMap.get(w2)));
        for(String each:myMap.keySet()) {
            help.offer(each);
            if(help.size()>k) {
                help.poll();
            }
        }
        List<String> out = new ArrayList<>();
        while(!help.isEmpty()) {
            out.add(help.poll());
        }
        Collections.reverse(out);
        return out;
    }
}
