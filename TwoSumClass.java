import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TwoSumClass {

    /** Initialize your data structure here. */
    HashMap<Integer,Integer> store;
    ArrayList<Integer> data;
    int index;

    public TwoSumClass() {
        store = new HashMap<>();
        data = new ArrayList<>();
        index = 0;
    }

    /** Add the number to an internal data structure.. */
    public void add(int number) {
        store.put(number,index);
        index++;
        data.add(number);
    }

    /** Find if there exists any pair of numbers which sum is equal to the value. */
    public boolean find(int value) {
       for(Integer each:data) {
           int other = value-each;
           if(store.containsKey(other) && store.get(other)!=data.indexOf(each)) {
               return true;
           }
       }
       return false;
    }

    public static void main(String[] args) {
        TwoSumClass a = new TwoSumClass();
        a.add(1);
        a.add(1);
        a.add(-1);
        a.add(-1);
        System.out.println(a.find(-2));
    }
}
