import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSums {

    //Brute-force
    public static int[] twoSumBF(int[] nums, int target) {
        int[] out = new int[2];
        for(int i=0;i<nums.length;i++) {
            for(int j=i+1;j<nums.length;j++) {
                if(nums[i]+nums[j] == target) {
                    out[0] = i;
                    out[1] = j;
                    return out;
                }
            }
        }
        return out;
    }

    //Optimal
    public static int[] twoSumOptimal(int[] nums, int target) {
        Map<Integer,Integer> numMap = new HashMap<>();
        int[] out = new int[2];
        for(int i = 0; i< nums.length; i++) {
            numMap.put(nums[i],i);
        }

       for(int i=0; i<nums.length;i++) {
           int reqNum = target - nums[i];
           if(numMap.containsKey(reqNum) && numMap.get(reqNum) != i) {
               out[0] = i;
               out[1] = numMap.get(reqNum);
               break;
           }
       }
       return out;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{12, 3, 4, 8, 10};
        System.out.println(Arrays.toString(twoSumOptimal(nums,18)));
    }
}
