import java.util.HashSet;
import java.util.Set;

public class UniqueEmail {
    public static int numUniqueEmails(String[] emails) {
        Set<String> uniqEmail = new HashSet<>();
        for(String each: emails) {
            int separation = each.indexOf("@");
            String local = each.substring(0,separation);
            String domain = each.substring(separation+1);
            if(local.contains("+")) {
                local = local.substring(0,local.indexOf("+"));
            }
            local = local.replaceAll("\\.","");
            uniqEmail.add(local+"@"+domain);
        }
        return uniqEmail.size();

    }

    public static void main(String[] args) {
        System.out.println(numUniqueEmails(new String[]{"testemail@leetcode.com","testemail1@leetcode.com","testemail+david@lee.tcode.com"}));
    }


}
