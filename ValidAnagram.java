import java.util.HashMap;
import java.util.Map;

public class ValidAnagram {
    public static boolean isAnagramLessComplicated(String s, String t) {
        if(s.length() != t.length()) {
            return false;
        }

        int[] counter = new int[26];
        for(int i=0;i<s.length();i++) {
            counter[s.charAt(i) - 'a']++;
            counter[t.charAt(i) - 'a']--;
        }

        for(int each:counter) {
            if(each != 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAnagram(String s, String t) {
        if(s.length() != t.length()) {
            return false;
        }
        Map<Character,Integer> anagramTracker = new HashMap<>();

        for(int i=0;i<s.length();i++) {
            anagramTracker.put(s.charAt(i), anagramTracker.getOrDefault(s.charAt(i),0) + 1);
        }

        for(int i=0;i<t.length();i++) {
            char key = t.charAt(i);
            if(anagramTracker.containsKey(key)) {
                int val = anagramTracker.get(key);
                if(val == 1) {
                    anagramTracker.remove(key);
                } else {
                    anagramTracker.put(key,--val);
                }
            } else {
                return false;
            }
        }

        if(anagramTracker.isEmpty()) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(isAnagramLessComplicated("ana","naa"));
    }
}
