public class ValidPalindrome {
    public static boolean isPalindromeOptimal(String s) {
        if(s==null || s.length()<2) {
            return true;
        }
        int start = 0;
        int end = s.length()-1;

        while(start<=end) {
            if(Character.isLetterOrDigit(s.charAt(start)) && Character.isLetterOrDigit(s.charAt(end))) {
                if(Character.toLowerCase(s.charAt(start)) != Character.toLowerCase(s.charAt(end))) {
                    return false;
                }
                start++;
                end--;
            } else if (Character.isLetterOrDigit(s.charAt(start)) && !Character.isLetterOrDigit(s.charAt(end))) {
                end--;
            }else if (!Character.isLetterOrDigit(s.charAt(start)) && Character.isLetterOrDigit(s.charAt(end))) {
                start++;
            } else {
                start++;
                end--;
            }
        }
        return true;
    }

    public static boolean isPalindrome(String s) {
        if(s== null || s.length()<2) {
            return true;
        }
        if(Character.isLetterOrDigit(s.charAt(0)) && Character.isLetterOrDigit(s.charAt(s.length()-1))) {
            if(Character.toLowerCase(s.charAt(0)) == Character.toLowerCase(s.charAt(s.length()-1))) {
                return isPalindrome(s.substring(1,s.length()-1));
            }
            return false;
        } else if (Character.isLetterOrDigit(s.charAt(0)) && !Character.isLetterOrDigit(s.charAt(s.length()-1))) {
            return isPalindrome(s.substring(0,s.length()-1));
        } else if (!Character.isLetterOrDigit(s.charAt(0)) && Character.isLetterOrDigit(s.charAt(s.length()-1))) {
            return isPalindrome(s.substring(1));
        } else {
            return isPalindrome(s.substring(1,s.length()-1));
        }
    }
    public static void main(String[] args) {
        System.out.println(isPalindromeOptimal("0P"));
    }
}
