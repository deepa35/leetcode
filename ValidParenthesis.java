import java.util.Stack;

public class ValidParenthesis {
    public static boolean isValid(String s) {
        Stack<Character> helper = new Stack<>();
        for(int i =0;i<s.length();i++) {
            if(s.charAt(i) == '(' || s.charAt(i) == '{' || s.charAt(i)=='[') {
                helper.push(s.charAt(i));
            } else {
                char prev = helper.isEmpty()? '$': helper.peek();
                if((s.charAt(i) == ')' && prev == '(') || (s.charAt(i) == '}' && prev == '{') || (s.charAt(i) == ']' && prev == '[')) {
                    helper.pop();
                } else {
                    return false;
                }
            }
        }
        return helper.isEmpty();
    }

    public static void main(String[] args) {
        System.out.println(isValid("{[]}"));
    }
}
