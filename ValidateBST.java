public class ValidateBST {
    public static boolean isValidBST(TreeNode root) {
        if(root==null) {
            return true;
        }
        return validBSTHelper(root,null,null);
    }

    private static boolean validBSTHelper(TreeNode node,Integer left,Integer right) {
        if(left!=null && node.val<=left) {
            return false;
        }
        if(right!=null && node.val>=right) {
            return false;
        }

        boolean l = node.left!=null?validBSTHelper(node.left,left,node.val):true;
        if(l==false) {
            return false;
        } else {
            boolean r = node.right!=null?validBSTHelper(node.right,node.val,right):true;
            return r;
        }
    }

    public static void main(String[] args) {
        TreeNode a = new TreeNode(2);
        a.left = new TreeNode(1);
        a.right = new TreeNode(3);

        System.out.println(isValidBST(a));
    }
}
