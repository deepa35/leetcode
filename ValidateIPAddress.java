public class ValidateIPAddress {
    public String validIPAddress(String IP) {
        if(isIPv4(IP)) {
            return "IPv4";
        } else if(isIPv6(IP)) {
            return "IPv6";
        }
        return "Neither";
    }

    private boolean isIPv4(String ip) {
        if(ip.length()<7) {
            return false;
        }
        if(ip.charAt(ip.length()-1)=='.') {
            return false;
        }
        String[] v4 = ip.split("\\.");
        if(v4.length!=4) {
            return false;
        }
        for(String each:v4) {
            if(each.length()<=0) {
                return false;
            }

            int val=-1;
            try{
                val=Integer.valueOf(each);
            } catch(NumberFormatException e) {
                return false;
            }
            if(each.startsWith("0") && (val!=0 || (val==0&&each.length()>1))) {
                return false;
            }
            if(val<0 || val>255) {
                return false;
            }
            if(val==0 && each.charAt(0) !='0') {
                return false;
            }
        }
        return true;
    }

    private boolean isIPv6(String ip) {
        if (ip.length() < 15) {
            return false;
        }
        if (ip.charAt(ip.length() - 1) == ':') {
            return false;
        }
        String[] v6 = ip.split(":");
        if (v6.length != 8) {
            return false;
        }
        for (String each : v6) {
            if (each.length() <= 0 || each.length() > 4) {
                return false;
            }
            char[] a = each.toCharArray();
            for (int i = 0; i < a.length; i++) {
                boolean isDig = Character.isDigit(a[i]);
                boolean ucAtoF = (a[i] >= 65 && a[i] <= 70) ? true : false;
                boolean lcAtoF = (a[i] >= 97 && a[i] <= 102) ? true : false;
                if (!isDig && !ucAtoF && !lcAtoF) {
                    return false;
                }
            }
        }
        return true;
    }
}
