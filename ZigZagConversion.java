public class ZigzagConversion {
    public static String convert(String s, int numRows) {
        if(numRows == 1) {
            return s;
        }

        String[] rowArr = new String[numRows];

        for(int i =0;i<rowArr.length;i++) {
            rowArr[i] = "";
        }

        boolean decreasing = false;
        int rowIndex=0;

        for(Character each:s.toCharArray()) {
            rowArr[rowIndex] = rowArr[rowIndex]+each;
            if(rowIndex == 0) {
                decreasing = false;
            } else if(rowIndex == numRows-1) {
                decreasing = true;
            }


            if (decreasing) {
                rowIndex--;
            } else {
                rowIndex++;
            }
        }

        String out = "";
        for(int i=0;i<rowArr.length;i++) {
            out+=rowArr[i];
        }
        return out;
    }

    public static void main(String[] args) {
        System.out.println(convert("AB",1));
    }
}
